/*@author 
Ferjad Naeem
Nouman Sial
code based on work from: gihan tharanga
*/

#include <iostream>
#include <string>
#include <algorithm>
//include opencv core
#include "opencv2\core\core.hpp"
#include "opencv2\contrib\contrib.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\objdetect\objdetect.hpp"
#include "opencv2\opencv.hpp"

//file handling
#include <fstream>
#include <sstream>

//rotates the image if no faces detected
#include"rotate.h"
using namespace std;
using namespace cv;

static Mat MatNorm(InputArray _src) {
	Mat src = _src.getMat();
	// Create and return normalized image:
	Mat dst;
	switch (src.channels()) {
	case 1:
		cv::normalize(_src, dst, 0, 255, NORM_MINMAX, CV_8UC1);
		break;
	case 3:
		cv::normalize(_src, dst, 0, 255, NORM_MINMAX, CV_8UC3);
		break;
	default:
		src.copyTo(dst);
		break;
	}
	return dst;
}

static void dbread(const string& filename, vector<Mat>& images, vector<int>& labels, char separator = ','){
	std::ifstream file(filename.c_str(), ifstream::in);

	if (!file){
		string error = "no valid input file";
		CV_Error(CV_StsBadArg, error);
	}

	string line, path, label;
	while (getline(file, line))
	{
		stringstream liness(line);
		getline(liness, path, separator);
		getline(liness, label);

		cout << endl << path << " " << label << endl;
		if (!path.empty() && !label.empty()){
			images.push_back(imread(path, 0));
			labels.push_back(atoi(label.c_str()));
		}
	}
}

void LBPHFaceTrainer(string csv, string yml){

	vector<Mat> images;
	vector<int> labels;

	try{
		string filename = csv;
		dbread(filename, images, labels);

		cout << "size of the images is " << images.size() << endl;
		cout << "size of the labes is " << labels.size() << endl;
		cout << "Training begins...." << endl;
	}
	catch (cv::Exception& e){
		cerr << " Error opening the file " << e.msg << endl;
		exit(1);
	}

	//lbph face recognier model
	Ptr<FaceRecognizer> model = createLBPHFaceRecognizer(1,12,8,8,DBL_MAX);

	//training images with relevant labels 
	model->train(images, labels);

	//save the data in yaml file
	model->save(yml);

	cout << "training finished...." << endl;

	waitKey(10000);
}

//lbpcascades works in lbphrecognier as fast as haarcascades 
int  FaceRecognition(string image, string yml, string outputcsv){

	vector<Point> points;

	cout << "start recognizing..." << endl;

	//load pre-trained data sets
	Ptr<FaceRecognizer>  model = createLBPHFaceRecognizer();
	model->load(yml);

	//lbpcascades/lbpcascade_frontalface.xml
	//string classifier = "C:/opencv/sources/data/haarcascades/haarcascade_frontalface_default.xml";
	string classifier = "C:/opencv/sources/data/haarcascades/haarcascade_frontalface_alt2.xml";
	

	CascadeClassifier face_cascade;
	string window = "Capture - face detection";
	//if (!face_cascade.load("C:/opencv/sources/data/haarcascades/haarcascade_frontalface_default.xml")){

	if (!face_cascade.load("C:/opencv/sources/data/haarcascades/haarcascade_frontalface_alt2.xml")){
		cout << " Error loading file" << endl;
		getchar();
		return -1;
	}
	namedWindow(window, 2);

	vector<Rect> faces;
	Mat frame;
	frame = imread(image, CV_LOAD_IMAGE_COLOR);
	int img_width = frame.cols;
	int img_height = frame.rows;
	Mat grayScaleFrame;

	cout << frame.empty();

	if (!frame.empty())
	{		//convert image to gray scale and equalize

		cvtColor(frame, grayScaleFrame, CV_RGB2GRAY);
		equalizeHist(grayScaleFrame, grayScaleFrame);

		//detect face in gray image
		face_cascade.detectMultiScale(grayScaleFrame, faces, 1.1, 4, CV_HAAR_DO_CANNY_PRUNING, cv::Size(.1*img_width, .1*img_height));

		//number of faces detected
		cout << faces.size() << " faces detected" << endl;

		int width = 0, height = 0;
		//region of interest

		//person name
		string Pname = "";
		int n = 0;
		while (faces.size() == 0 && n < 4){								//counts the no of rotations needed to detect a face
			grayScaleFrame = rotate_cw(grayScaleFrame, 90);
			frame = rotate_cw(frame, 90);								//rotates coloured "frame" to orintation of  greyscaleframe 
			face_cascade.detectMultiScale(grayScaleFrame, faces, 1.1, 4, 1, cv::Size(100, 100));
			n++;
		}
		cout << "Image rotated " << n << " times" << endl;

		cout << faces.size();
		for (int i = 0; i < faces.size(); i++)
		{
			//region of interest
			Rect face_i = faces[i];

			//crop the roi from grya image
			Mat face = grayScaleFrame(face_i);

			//resizing the cropped image to suit to database image sizes
			Mat face_resized;
			cv::resize(face, face_resized, Size(img_width, img_height), 1.0, 1.0, INTER_CUBIC);

			//recognizing what faces detected
			int label = -1; double confidence = 0;

			model->predict(face_resized, label, confidence);

			cout << " confidence " << confidence << endl;

			//drawing green rectagle in recognize face
			rectangle(frame, face_i, CV_RGB(0, 255, 0), 1);
			if (confidence > 140)
			{
				cout << endl << "Confidence greater than threshold" << endl;
				Pname = "unknown";
			}
			else{
				switch (label){
				case(1) :
					Pname = "Abdullah afzal";
					break;
				case(2) :
					Pname = "talha bin asif";
					break;
				case(3) :
					Pname = "Ahmad mustafa";
					break;
				case(4) :
					Pname = "suhaib bajwa";
					break;
				case(5) :
					Pname = "Nouman sial";
					break;
				case(6) :
					Pname = "Obaid satti";
					break;
				case(7) :
					Pname = "training7";
					break;
				case(8) :
					Pname = "Saim shujaat";
					break;
				case(9) :
					Pname = "Nouraiz hussain";
					break;
				case(10) :
					Pname = "Pathan Bhai";
					break;
				default:
					Pname = "unknown";
					break;
				}
			}
			cout << "label is " << label << endl;
			int pos_x = std::max(face_i.tl().x - 10, 0);
			int pos_y = std::max(face_i.tl().y - 10, 0);

			putText(frame, "Detected: " + Pname, Point(pos_x, pos_y), FONT_HERSHEY_COMPLEX_SMALL, 1.0, CV_RGB(0, 255, 0), 1.0);
			
			
		}


		//display to the winodw
		cv::imshow(window, frame);
		waitKey(1000);
				
		system("pause");
		
		ofstream outfile;
		
		outfile.open(outputcsv, ios::app);
		outfile << image << ",";
		outfile << Pname << endl;
		outfile.close();
		exit(0);
	}
	
}
