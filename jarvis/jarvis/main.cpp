/*@author Ferjad Naeem, Nouman Sial*/

#include <iostream>
#include <string>

#include "opencv2\core\core.hpp"
#include "opencv2\contrib\contrib.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\objdetect\objdetect.hpp"
#include "opencv2\opencv.hpp"

/*header files*/
#include "FaceRec.h"
#include "imgProc.h"

using namespace std;
using namespace cv;

int main(int argc, char **argv)
{
	if (argc == 1)
	{
		cout << "Usage:" << endl; 
		cout << "Training: [jarvis.exe] [train] [path to csv] [path to output yml]" << endl;
		cout << "Recognition: [jarvis.exe] [recog] [path to image] [path to yml]" << endl;
		cin.ignore();
		exit(0);
	}
	if (strcmp(argv[1], "train") == 0){
		LBPHFaceTrainer(argv[2], argv[3]);
	}
	if (strcmp(argv[1],"recog")==0){
		if (argc==4)
		{
			argv[4] = "result.csv";
		}
		FaceRecognition(argv[2], argv[3],argv[4]);
	}
	//int value = FaceRecognition();




	cin.ignore();
	return 0;
}