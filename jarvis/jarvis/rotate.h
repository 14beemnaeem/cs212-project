#include <iostream>
#include <string>
#include <algorithm>
//include opencv core
#include "opencv2\core\core.hpp"
#include "opencv2\contrib\contrib.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\objdetect\objdetect.hpp"
#include "opencv2\opencv.hpp"

//file handling
#include <fstream>
#include <sstream>

using namespace std;
using namespace cv;
cv::Mat
rotate_cw(const cv::Mat& image, int degrees)
{
	cv::Mat res;
	switch (degrees % 360) {
	case 0:
		res = image;
		break;
	case 90:
		res = image.t();
		cv::flip(res, res, 1);
		break;
	case 180:
		cv::flip(image, res, -1);
		break;
	case 270:
		res = image.t();
		cv::flip(res, res, 0);
		break;
	default:
		cv::Mat r = cv::getRotationMatrix2D({ image.cols / 2.0F, image.rows / 2.0F }, degrees, 1.0);
		int len = std::max(image.cols, image.rows);
		cv::warpAffine(image, res, r, cv::Size(len, len));
		break; //image size will change
	}
	return res;
}
//void output(){
//}